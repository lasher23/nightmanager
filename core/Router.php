<?php

class Router
{
    private $routes;

    function __construct($routes)
    {
        $this->routes = $routes;
    }

    public function getRoute($route) : string
    {
        if(array_key_exists($route,$this->routes)){
            return $this->routes[$route];
        }
        http_response_code(404);
        die();
    }
}