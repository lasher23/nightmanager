<?php
$matchId = $_POST['match-id'];
$teamId = $_POST['team-id'];

$match = Match::getById($matchId);
if ($teamId == $match->getTeamGuest()->getId()) {
    $match->setScoreTeamGuest($match->getScoreTeamGuest() - 1);
    $match->save();
} else if ($teamId == $match->getTeamHome()->getId()) {
    $match->setScoreTeamHome($match->getScoreTeamHome() - 1);
    $match->save();
}

header('Location:/Match?id=' . $matchId);