<?php
$matchId = $_GET['id']??'';
$all = $_GET['all']??false;
if ($matchId != '') {
    $match = Match::getById($matchId);
    require 'app/view/match.view.php';
} else {
    if ($_SESSION['role'] == 'refree') {
        if (isset($_SESSION['hall'])) {
            $matchesPerHall = [];
            $match = Match::getNearestMatchesByHall($_SESSION['hall']);
            array_push($matchesPerHall, $match);
            if (count($matchesPerHall) > 0) {
                require 'app/view/matchoverview.view.php';
            } else {
                require 'app/view/nomatches.view.php';
            }
        } else {
            require 'app/controller/HallChooserController.php';
        }
    } else {
        $halls = Hall::getAll();
        $matchesPerHall = [];
        foreach ($halls as $hall) {
            if ($all || $_SESSION['role'] == 'admin' || $_SESSION['role'] == 'display') {
                array_push($matchesPerHall, Match::getMatchesByHallId($hall->getId()));
            } else {
                $matches = Match::getNearestMatchesByHall($hall->getId());
                if (count($matches) > 0) {
                    array_push($matchesPerHall, $matches);
                }
            }
        }
        if (count($matchesPerHall) > 0) {
            require 'app/view/matchoverview.view.php';
        } else {
            require 'app/view/nomatches.view.php';
        }
    }
}