<?php
$id = $_POST['hall-id']??'';
if ($id != '') {
    $_SESSION['hall'] = $id;
    require 'app/controller/MatchController.php';
} else {
    $halls = Hall::getAll();
    require 'app/view/hallchooser.view.php';
}