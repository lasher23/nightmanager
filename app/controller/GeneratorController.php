<?php
$type = strtolower($_GET['type'])??'';
$id = $_POST['id']??'';
$categories = Category::getAll();
if ($id !== '') {
    $category = Category::getById($id);
    if ($type == 'semifinal') {
        if ($category->getPartnerCategory() != null) {
            PartnerCategorySemifinalGenerator::generate($category);
        } else {
            $semisgenerator = new Semifinalgenerator($category);
            $semisgenerator->generate();
        }
    } elseif ($type == 'final') {
        if ($category->isOnlyFinal()) {
            DirectFinalGenerator::generate($category);
        } else {
            $finalgenerator = new Finalgenerator($category);
            $finalgenerator->generate();
        }
    } elseif ($type == 'croki') {
        CrokiCupGenerator::generate(Category::getById($id));
    }
}
require 'app/view/generator.view.php';
