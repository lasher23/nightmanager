<?php
$categoryId = $_GET['categoryId']??'';
if ($categoryId !== '') {
    $category = Category::getById($categoryId);
    $teams = Team::getTeamsOfCategory($categoryId);
    if (sizeof($teams) > 0) {
        require 'app/view/category.view.php';
    }
} else {
    $categories = Category::getAll();
    require 'app/view/categoryoverview.view.php';
}
