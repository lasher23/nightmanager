<?php
$matchId = $_POST['match-id']??'';

if ($matchId !== '') {
    $match = Match::getById($matchId);
    if (!$match->isFinished()) {
        if ($match->getisGroupgame()) {
            $pointcalculator = new PointCalculator($match);
            $pointcalculator->calculatePoints();
            GoaldifferenceCalculator::calculate($match);
        }
        $match->setIsFinished(true);
        $match->save();
        mail('uhcyetis@gmail.com', 'Match' . $match->getTeamHome()->getName() . ':' . $match->getTeamGuest()->getName(), $match->getTeamHome()->getName() . ' ' . $match->getScoreTeamHome() . ':' . $match->getScoreTeamGuest() . ' ' . $match->getTeamGuest()->getName());
    }
}

require 'app/controller/MatchController.php';