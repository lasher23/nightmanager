<?php

class Match implements Entity
{
    private $id = 0;
    private $TeamHome = null;
    private $TeamGuest = null;
    private $ScoreTeamHome = 0;
    private $ScoreTeamGuest = 0;
    private $isGroupgame = true;
    private $hall = null;
    private $gametime = null;
    private $isQuartergame = false;
    private $isFinal = false;
    private $isFinished = false;

    private function __construct($id = 0, $TeamHome = null, $TeamGuest = null, $ScoreTeamHome = 0, $ScoreTeamGuest = 0, $isGroupgame = true, $hall = null, $gametime = null, $isQuartergame = false, $isFinal = false, $isFinished = false)
    {
        $this->id = $id;
        $this->TeamHome = $TeamHome;
        $this->TeamGuest = $TeamGuest;
        $this->ScoreTeamHome = $ScoreTeamHome;
        $this->ScoreTeamGuest = $ScoreTeamGuest;
        $this->isGroupgame = $isGroupgame;
        $this->hall = $hall;
        $this->gametime = DateTime::createFromFormat('H:i:s', $gametime);
        $this->isFinished = $isFinished;
        if ($this->getisGroupgame() == false) {
            $this->isQuartergame = $isQuartergame;
        } elseif (!$this->isQuartergame()) {
            $this->isFinal = $isFinal;
        }
        if (!$this->isFinal() && !$this->getisGroupgame() && !$this->isQuartergame) {
            $isGroupgame == true;
        }
    }

    public function isFinished(): bool
    {
        return $this->isFinished;
    }

    public function setIsFinished(bool $isFinished)
    {
        $this->isFinished = $isFinished;
    }

    public static function getMatchFromArray($match): Match
    {
        $matchobj = new Match();
        $matchobj->setIsFinished($match['isFinished']);
        $matchobj->setId($match['id']);
        $matchobj->setTeamHome(Team::getById($match['fk_Team1']));
        $matchobj->setTeamGuest(Team::getById($match['fk_Team2']));
        $matchobj->setScoreTeamHome($match['ScoreTeam1']);
        $matchobj->setScoreTeamGuest($match['ScoreTeam2']);
        $matchobj->setIsGroupgame($match['isGroupgame']);
        $matchobj->setHall(Hall::getById($match['fk_hall']));
        $matchobj->setGametime(DateTime::createFromFormat('H:i:s', $match['gametime']));
        $matchobj->setIsQuartergame($match['isQuartergame']);
        $matchobj->setIsFinal($match['isFinal']);
        return $matchobj;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTeamHome(): Team
    {
        return $this->TeamHome;
    }

    public function getTeamGuest(): Team
    {
        return $this->TeamGuest;
    }

    public function getScoreTeamGuest(): int
    {
        return $this->ScoreTeamGuest;
    }

    public function getScoreTeamHome(): int
    {
        return $this->ScoreTeamHome;
    }

    public function getisGroupgame(): int
    {
        return $this->isGroupgame;
    }

    public function getHall()
    {
        return $this->hall;
    }

    public function getGametime(): DateTime
    {
        return $this->gametime;
    }

    public function isFinal(): bool
    {
        return $this->isFinal;
    }

    public function isQuartergame(): bool
    {
        return $this->isQuartergame;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function setTeamHome(Team $TeamHome)
    {
        $this->TeamHome = $TeamHome;
    }

    public function setTeamGuest(Team $TeamGuest)
    {
        $this->TeamGuest = $TeamGuest;
    }

    public function setScoreTeamHome(int $ScoreTeamHome)
    {
        $this->ScoreTeamHome = $ScoreTeamHome;
    }

    public function setScoreTeamGuest(int $ScoreTeamGuest)
    {
        $this->ScoreTeamGuest = $ScoreTeamGuest;
    }

    public function setIsGroupgame(bool $isGroupgame)
    {
        $this->isGroupgame = $isGroupgame;
    }

    public function setHall(Hall $hall)
    {
        $this->hall = $hall;
    }

    public function setGametime($gametime)
    {
        $this->gametime = $gametime;
    }

    public function setIsQuartergame(bool $isQuartergame)
    {
        $this->isQuartergame = $isQuartergame;
    }

    public function setIsFinal(bool $isFinal)
    {
        $this->isFinal = $isFinal;
    }


    static function createNew(): Match
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call createNewMatch()');
        $statement->execute();
        return self::getById($dbh->lastInsertId());
    }

    function save()
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call updateMatch(' . $this->getId() . ', ' . $this->getTeamHome()->getId() . ', ' . $this->getTeamGuest()->getId() . ', ' . $this->getHall()->getId() . ', ' . $this->getisGroupgame() . ', ' . $this->getScoreTeamHome() . ', ' . $this->getScoreTeamGuest() . ', ' . (int)$this->isFinished() . ')');
        $statement->execute();
    }

    static function getAll(): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getAllMatches()');
        $statement->execute();
        $array = $statement->fetchAll();
        $matches = [];
        foreach ($array as $match) {
            array_push($matches, new Match($match['id'], Team::getById($match['fk_Team1']), Team::getById($match['fk_Team2']), $match['ScoreTeam1'], $match['ScoreTeam2'], $match['isGroupgame'], Hall::getById($match['fk_hall'])));
        }

        return $matches;
    }

    static function getMatchesByHallAndCategoryInGroupgame($hallId, $categoryId): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getMatchesByHallAndCategory(:hallId, :categoryId)');
        $statement->bindParam(':hallId', $hallId);
        $statement->bindParam(':categoryId', $categoryId);
        $statement->execute();
        $array = $statement->fetchAll();
        $matches = [];
        foreach ($array as $match) {
            array_push($matches, self::getMatchFromArray($match));
        }
        return $matches;
    }

    static function getMatchesByHallId($hallId): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getAllMatchesByHall(:hallId)');
        $statement->bindParam(':hallId', $hallId);
        $statement->execute();
        $array = $statement->fetchAll();
        $matches = [];
        foreach ($array as $match) {
            $matchobj = self::getMatchFromArray($match);
            array_push($matches, $matchobj);
        }
        return $matches;
    }

    static function getById($id)
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getMatchById(:id)');
        $statement->bindParam(':id', $id);
        $statement->execute();
        $match = $statement->fetch();
        $matchobj = self::getMatchFromArray($match);
        return $matchobj;
    }

    static function getQuarterfinalsOfCategory(Category $category): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getQuarterfinalsOfCategory(' . $category->getId() . ')');
        $statement->execute();
        $array = $statement->fetchAll();

        $matches = [];
        foreach ($array as $match) {
            $matchobj = self::getMatchFromArray($match);
            array_push($matches, $matchobj);
        }
        return $matches;
    }

    static function getNearestMatchesByHall(int $hallid)
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getNearestMatchesByHall(:hallId)');
        $statement->bindParam(':hallId', $hallid);
        $statement->execute();
        $array = $statement->fetchAll();
        $matches = [];
        foreach ($array as $match) {
            $matchobj = self::getMatchFromArray($match);
            array_push($matches, $matchobj);
        }
        return $matches;
    }

    static function getFinalOfCategory(Category $category): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getFinalOfCategory(' . $category->getId() . ')');
        $statement->execute();
        $array = $statement->fetchAll();
        $matches = [];
        foreach ($array as $match) {
            array_push($matches, self::getMatchFromArray($match));
        }
        return $matches;
    }
}