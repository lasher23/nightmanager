<?php

class Category implements Entity
{
    private $id;
    private $name;
    private $partnerCategory;
    private $isCroki;
    private $crokiCategory;
    private $onlyFinal;

    private function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCrokiCategory(): ?Category
    {
        return $this->crokiCategory;
    }

    public function getPartnerCategory(): ?Category
    {
        return $this->partnerCategory;
    }

    public function isCroki(): bool
    {
        return $this->isCroki;
    }

    public function isOnlyFinal(): bool
    {
        return $this->onlyFinal;
    }

    public function setIsOnlyFinal(bool $isOnlyFinal)
    {
        $this->onlyFinal = $isOnlyFinal;
    }

    function save()
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call updateCategory(:id,:name)');
        $statement->bindParam(':id', $this->getId());
        $statement->bindParam(':name', $this->getName());
        $statement->execute();
    }

    static function createNew(): Category
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call createNewCategory()');
        $statement->execute();
        return self::getById($dbh->lastInsertId());
    }

    function __construct($id = 0, $name = '', Category $partnerCategory = null, bool $isCroki = false, Category $crokiCateogry = null, $isOnlyFinal = false)
    {
        $this->id = $id ?? '';
        $this->name = $name ?? '';
        $this->partnerCategory = $partnerCategory;
        $this->isCroki = $isCroki;
        $this->crokiCategory = $crokiCateogry;
        $this->isOnlyFinal = $isOnlyFinal;
    }

    static function getAll(): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare("call getAllCategories()");
        $statement->execute();
        $array = $statement->fetchAll();
        $categories = [];

        foreach ($array as $category) {
            array_push($categories, self::getCategoryObjectByArray($category));
        }
        return $categories;
    }

    static function getById($id): ?Category
    {
        if ($id == null || $id == 0) {
            return null;
        }

        $dbh = getConection();
        $statement = $dbh->prepare("call getCategoryById(:id)");
        $statement->bindParam(':id', $id);
        $statement->execute();
        $array = $statement->fetch();

        $category = self::getCategoryObjectByArray($array);

        return $category;
    }

    static function getCategoriesByCrokiParent($crokiParentId): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare("call getCategoriesByCrokiParent(:crokiParentId)");
        $statement->bindParam(':crokiParentId', $crokiParentId);
        $statement->execute();
        $array = $statement->fetchAll();

        $categories = [];

        foreach ($array as $category) {
            array_push($categories, self::getCategoryObjectByArray($category));
        }
        return $categories;
    }

    private static function getCategoryObjectByArray($array): Category
    {
        $category = new Category();
        $category->setId((int)$array['id']);
        $category->setName($array['name']);
        $category->setPartnerCategory(Category::getById($array['fk_partnerCategory']));
        $category->setIsCroki($array['isCroki']);
        $category->setCrokiCategory(Category::getById($array['fk_CrokiCategory']));
        $category->setIsOnlyFinal((bool)$array['onlyFinal']);
        return $category;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setPartnerCategory(?Category $partnerCategory)
    {
        $this->partnerCategory = $partnerCategory;
    }

    public function setIsCroki(bool $isCroki)
    {
        $this->isCroki = $isCroki;
    }

    public function setCrokiCategory(?Category $crokiCategory)
    {
        $this->crokiCategory = $crokiCategory;
    }
}