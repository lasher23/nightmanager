<?php

class Team implements Entity
{
    private $id = 0;
    private $name = '';
    private $category = null;
    private $points = 0;
    private $goalDifference = 0;

    private function __construct($id = 0, $name = '', $points = 0, $category = null, $goalDifference = 0)
    {
        $this->id = $id ?? '';
        $this->name = $name ?? '';
        $this->category = $category ?? null;
        $this->points = $points;
        $this->goalDifference = $goalDifference;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }
    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getPoints(): int
    {
        return $this->points;
    }

    public function setPoints(int $points)
    {
        $this->points = $points;
    }

    public function getGoalDifference(): int
    {
        return $this->goalDifference;
    }

    public function setGoalDifference(int $goalDifference)
    {
        $this->goalDifference = $goalDifference;
    }

    function save()
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call updateTeam(' . $this->getId() . ', \'' . $this->getName() . '\', ' . $this->getPoints() . ', ' . $this->getCategory()->getId() . ', '.$this->getGoalDifference().')');
        $statement->execute();
    }

    static function createNew(): Team
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call createNewTeam()');
        $statement->execute();
        return self::getById($dbh->lastInsertId());
    }

    public static function getTeamsOfCategory($categoryId)
    {
        $dbh = getConection();
        $statement = $dbh->prepare("call getTeamsByCategory(:categoryId)");
        $statement->bindParam(':categoryId', $categoryId);
        $statement->execute();
        $array = $statement->fetchAll();
        $teams = [];
        foreach ($array as $value) {
            array_push($teams, new Team($value['id'], $value['name'], $value['points'], Category::getById($value['fk_category']), $value['GoalDifference']));
        }
        return $teams;
    }

    public static function getById($id): Team
    {
        $dbh = getConection();
        $statement = $dbh->prepare("call getTeamById(:id)");
        $statement->bindParam(':id', $id);
        $statement->execute();
        $array = $statement->fetch();
        $team = new Team();

        $team->id = $array['id'];
        $team->name = $array['name'];
        $team->points = $array['points'];
        $team->goalDifference = $array['GoalDifference'];
        $team->category = Category::getById($array['fk_category']);

        return $team;
    }

    static function getAll(): array
    {
        // TODO: Implement getAll() method.
    }

    public function getJsonData()
    {
        $var = get_object_vars($this);
        foreach ($var as &$value) {
            if (is_object($value) && method_exists($value, 'getJsonData')) {
                $value = $value->getJsonData();
            }
        }
        return $var;
    }
}