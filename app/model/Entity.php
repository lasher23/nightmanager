<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 01.11.2017
 * Time: 20:20
 */
interface Entity
{
    function save();

    static function createNew();

    static function getAll(): array;

    static function getById($id);
}