<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 12.10.2017
 * Time: 19:40
 */
class Hall implements Entity
{
    private $id;
    private $name;

    function __construct($id = 0, $name = '')
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    static function createNew()
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call createNewHall()');
        $statement->execute();
        return self::getById($dbh->lastInsertId());
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function save()
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call updateHall(:id,:name)');
        $statement->bindParam(':name', $this->name);
        $statement->bindParam(':id', $this->id);
        $statement->execute();
    }

    static function getById($id): Hall
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getHallbyId(:id)');
        $statement->bindParam(':id', $id);
        $statement->execute();
        $array = $statement->fetch();
        return new Hall($array['id'], $array['name']);
    }

    static function getAll(): array
    {
        $dbh = getConection();
        $statement = $dbh->prepare('call getAllHalls()');
        $statement->execute();
        $array = $statement->fetchAll();
        $halls = [];
        foreach ($array as $hall) {
            array_push($halls, new Hall($hall['id'], $hall['name']));
        }
        return $halls;
    }
}