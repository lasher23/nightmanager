<?= firstPart() ?>
    <div class="jumbotron"><h2>Halle wählen</h2></div>
    <div>
        <form method="post" href="HallChooser">
            <?php foreach ($halls as $hall) { ?>
                <div class="form-group row">
                    <button type="submit" class="btn col-xs-6" name="hall-id"
                            value="<?= $hall->getId() ?>"><?= $hall->getName() ?></button>
                </div>
            <?php } ?>
        </form>
    </div>
<?= secondPart() ?>