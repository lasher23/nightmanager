<?= firstPart() ?>
<div class="jumbotron">
    <h2>Kategorien Übersicht</h2>
</div>
<table class="table">
    <thead>
    <th>Name</th>
    <th>Identifikator</th>
    </thead>
    <tbody>
    <?php foreach ($categories as $category) { ?>
        <tr class="clickable-row" data-href="Category?categoryId=<?= $category->getId()?>">
            <td><?= $category->getName()?></td>
            <td><?= $category->getId()?></td>
        </tr>
    <?php } ?>
    </tbody >
</table>
<script>
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            console.log($(this).data("href"))
            window.location = $(this).data("href");
        });
    });
</script>
<?= secondPart() ?>

