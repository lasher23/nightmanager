<?= firstPart() ?>
    <div class="jumbotron">
        <h2><?= $category->getName() ?></h2>
    </div>
    <div>
        <table class="table">
            <thead>
            <th>Rang</th>
            <th>Team</th>
            <th>Punkte</th>
            <th>Tor Verhältnis</th>
            </thead>
            <tbody>
            <?php $i = 0;
            foreach ($teams as $team) {
                $i++; ?>
                <tr>
                    <td>
                        <?= $i ?>
                    </td>
                    <td>
                        <?= $team->getName() ?>
                    </td>
                    <td>
                        <?= $team->getPoints() ?>
                    </td>
                    <td>
                        <?= $team->getGoalDifference()?>
                    </td>
                </tr>
            <?php }; ?>
            </tbody>
        </table>
    </div>
<?= secondPart() ?>