<?php
function firstPart()
{
    $standard =
    $nav = $_GET['nav']??'';
    if ($nav || $_SESSION['role'] === 'admin') {
        return '
            <html>
             <head>
                <title>NightManager</title>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta http-equiv="Refresh" content="60">
                    <link rel="stylesheet" href="public/css/bootstrap.min.css">
                    <script src="public/js/jquery-3.2.1.min.js"></script>
                    <script src="public/js/bootstrap.min.js"></script>
                </head>
            <body>
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Night Manager</a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="Match">Spiele</a></li>
                            <li><a href="Category">Kategorie</a></li>
                            <li><a href="Generator">Generierung</a></li>
                        </ul>
                    </div>
                </nav>
                <div class="container">
';
    } else {
        return '
            <html>
             <head>
                <title>NightManager</title>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta http-equiv="Refresh" content="60">
                    <link rel="stylesheet" href="public/css/bootstrap.min.css">
                    <script src="public/js/jquery-3.2.1.min.js"></script>
                    <script src="public/js/bootstrap.min.js"></script>
                </head>
            <body>
                <div class="container">';
    }
}

function secondPart()
{
    return '</div>
    <div class="footer navbar-fixed-bottom">
    <a class="center-block btn" href="Logout"><img src="public/images/logo.png" style="width: 30%"></a>
</div>
</div>
</body>
</html>';
}
