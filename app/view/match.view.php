<?= firstPart() ?>
    <div class="jumbotron">
        <h4 class="visible-xs" style="text-align: center"><?= $match->getTeamHome()->getName() ?>
            : <?= $match->getTeamGuest()->getName() ?></h4>
        <h2 class="visible-lg visible-md visible-sm" style="text-align: center"><?= $match->getTeamHome()->getName() ?>
            : <?= $match->getTeamGuest()->getName() ?></h2>
        <h4 style="text-align: center"><?= $match->getTeamGuest()->getCategory()->getName()?></h4>
    </div>
    <div>
        <div class="col-xs-6 col-md-6">
            <h2 class="text-center"><?= $match->getScoreTeamHome() ?></h2>
            <form method="post">
                <input type="hidden" name="team-id" value="<?= $match->getTeamHome()->getId() ?>">
                <input type="hidden" name="match-id" value="<?= $match->getId() ?>">
                <button type="submit"
                        class="btn btn-primary btn-lg btn-group-justified <?php if ($match->isFinished()) {
                            echo 'disabled';
                        } ?>" formaction="IncreaseScore"
                    <?php if ($match->isFinished()) {
                        echo 'disabled';
                    } ?>
                        style="height: 8em">+
                </button>
                <button type="submit" class="btn btn-primary center-block <?php if ($match->isFinished()) {
                    echo 'disabled';
                } ?>" formaction="DecreaseScore"
                    <?php if ($match->isFinished()) {
                        echo 'disabled';
                    } ?>
                        style="margin-top: 1em">-
                </button>
            </form>
        </div>
        <div class="col-xs-6 col-md-6">
            <h2 class="text-center"><?= $match->getScoreTeamGuest() ?></h2>
            <form method="post">
                <input type="hidden" name="team-id" value="<?= $match->getTeamGuest()->getId() ?>">
                <input type="hidden" name="match-id" value="<?= $match->getId() ?>">
                <button type="submit"
                        class="btn btn-primary btn-lg btn-group-justified <?php if ($match->isFinished()) {
                            echo 'disabled';
                        } ?>" formaction="IncreaseScore"
                    <?php if ($match->isFinished()) {
                        echo 'disabled';
                    } ?>
                        style="height: 8em">+
                </button>
                <button type="submit" class="btn btn-primary center-block <?php if ($match->isFinished()) {
                    echo 'disabled';
                } ?>" formaction="DecreaseScore"
                    <?php if ($match->isFinished()) {
                        echo 'disabled';
                    } ?>
                        style="margin-top: 1em">-
                </button>
            </form>
        </div>
        <form method="post" class="center-block">
            <input type="hidden" name="match-id" value="<?= $match->getId() ?>">
            <div class="btn-group center-block">
                <?php if ($match->getisGroupgame()) { ?>
                    <button type="submit" class="btn btn center-block text-center <?php if (!$match->isFinished()) {
                        echo 'disabled';
                    } ?>" formaction="<?php if ($match->isFinished()) {
                        echo 'UndoMatch';
                    } ?>" style="height: 4em;">Zur�cksetzen
                    </button>
                <?php } ?>
                <button type="submit"
                        class="btn center-block text-center <?php if ($match->isFinished()) {
                            echo 'disabled';
                        } ?>" formaction="<?php if (!$match->isFinished()) {
                    echo 'FinishMatch';
                } ?>" style="height: 4em; margin-left: 1em;">Abschliessen
                </button>
                <button class="btn center-block text-center" style="height: 4em; margin-left: 1em;" formaction="Match">
                    Zur�ck
                </button>
            </div>
        </form>
    </div>
<?= secondPart() ?>