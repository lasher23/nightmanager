<?= firstPart() ?>
    <div class="jumbotron">
        <h2>Generierung</h2>
    </div>
    <div>
        <table>
            <?php foreach ($categories as $category) { ?>
                <tr>
                    <td>
                        <form class="form-inline" method="post">
                            <input type="number" class="form-control input-sm" style="border: 0;" readonly
                                   value="<?= $category->getId() ?>" name="id">
                            <input type="text" class="form-control input-sm" style="border: 0;" readonly
                                   value="<?= $category->getName() ?>">
                            <?php if (!$category->isOnlyFinal()) { ?>
                                <button type="submit" class="btn" formaction="/Generator?type=Semifinal">
                                    Generierung Halbfinale
                                </button>
                            <?php } ?>
                            <button type="submit" class="btn" formaction="/Generator?type=Final">
                                Generierung Finale
                            </button>
                            <?php if ($category->isCroki() && $category->getCrokiCategory() == null) { ?>
                                <button type="submit" class="btn" formaction="/Generator?type=Croki">
                                    Generierung Croki
                                </button><?php } ?>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?= secondPart() ?>