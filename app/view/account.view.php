<?= firstPart() ?>
    <div class="jumbotron">
        <h2>Art wählen</h2>
    </div>
    <span style="color: darkred"><?php
        if (isset($error)) {
            echo $error;
        } ?></span>
    <form method="post" href="">
        <div class="form-group">
            <label for="role">Rolle auswählen:</label>
            <select class="form-control" id="role" name="role">
                <option value="refree">Schiedsrichter</option>
                <option value="display">Anzeige</option>
                <option value="admin">Admin</option>
            </select>
        </div>
        <div class="form-group">
            <label for="pwd">Passwort:</label>
            <input type="password" class="form-control" id="pwd" name="pwd">
        </div>
        <button type="submit" class="btn btn-default">Einloggen</button>
    </form>
<?= secondPart() ?>