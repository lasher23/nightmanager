<?= firstPart() ?>
    <div class="jumbotron">
        <h2>Spiele</h2>
        <?php if ($_SESSION['role'] == 'refree') { ?>
            <a href="HallChooser" class="btn btn-info" role="button">Halle wechseln</a>
        <?php } ?>
    </div>
<?php foreach ($matchesPerHall as $hallPosibility) { ?>
    <div class="table-responsive col-md-6 col-lg-6">
        <h1><?= $hallPosibility[0]->getHall()->getName() ?></h1>
        <table class="table">
            <thead>
            <th>Zeit</th>
            <th>Heim</th>
            <th>Resultat</th>
            <th>Gast</th>
            </thead>
            <tbody>
            <?php foreach ($hallPosibility as $match) { ?>
                <tr class="clickable-row" data-href="Match?id=<?= $match->getId() ?>">
                    <td><?= $match->getGametime()->format('H:i') ?></td>
                    <td><?= $match->getTeamHome()->getName() ?></td>
                    <td><?= $match->getScoreTeamHome() . ' : ' . $match->getScoreTeamGuest() ?></td>
                    <td><?= $match->getTeamGuest()->getName() ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
    <script>
        $(document).ready(function ($) {
            $(".clickable-row").click(function () {
                console.log($(this).data("href"))
                window.location = $(this).data("href");
            });
        });
    </script>
<?= secondPart() ?>