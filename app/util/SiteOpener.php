<?php

class SiteOpener
{
    public static function openUserSpecificDefaultSite($user)
    {
        if ($user === 'refree') {
            require 'app/controller/MatchController.php';
        } elseif ($user === 'admin') {
            require 'app/controller/GeneratorController.php';
        } elseif ($user === 'display') {
            require 'app/controller/CategoryController.php';
        }
    }
}