<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 01.12.2017
 * Time: 20:57
 */
class ScoreUndoer
{
    public static function undoScore(Match $match)
    {
        $teamGuest = $match->getTeamGuest();
        $teamHome = $match->getTeamHome();
        $scoreGuest = $match->getScoreTeamGuest();
        $scoreHome = $match->getScoreTeamHome();
        self::undoPoints($scoreGuest, $scoreHome, $teamGuest, $teamHome);
        self::undoGoalDifference($scoreGuest, $scoreHome, $teamGuest, $teamHome);
        $match->setScoreTeamHome(0);
        $match->setScoreTeamGuest(0);
        $match->save();
    }

    public static function undoPoints(int $scoreGuest, int $scoreHome, Team $teamGuest, Team $teamHome): void
    {
        if ($scoreGuest > $scoreHome) {
            $teamGuest->setPoints($teamGuest->getPoints() - 2);
        } elseif ($scoreGuest < $scoreHome) {
            $teamHome->setPoints($teamHome->getPoints() - 2);
        } else {
            $teamGuest->setPoints($teamGuest->getPoints() - 1);
            $teamHome->setPoints($teamHome->getPoints() - 1);
        }
        $teamGuest->save();
        $teamHome->save();
    }

    public static function undoGoalDifference(int $scoreGuest, int $scoreHome, Team $teamGuest, Team $teamHome): void
    {
        $differenceTeamHome = $scoreHome - $scoreGuest;
        $differenceTeamGuest = $scoreGuest - $scoreHome;
        $teamHome->setGoalDifference($teamHome->getGoalDifference() - $differenceTeamHome);
        $teamGuest->setGoalDifference($teamGuest->getGoalDifference() - $differenceTeamGuest);
        $teamHome->save();
        $teamGuest->save();
    }
}