<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 29.11.2017
 * Time: 22:08
 */
class GoaldifferenceCalculator
{
    public static function calculate(Match $match)
    {
        $teamHome = $match->getTeamHome();
        $teamGuest = $match->getTeamGuest();

        $differenceTemHome = $match->getScoreTeamHome() - $match->getScoreTeamGuest();
        $differenceTemGuest = $match->getScoreTeamGuest() - $match->getScoreTeamHome();

        $teamHome->setGoalDifference($teamHome->getGoalDifference() + $differenceTemHome);
        $teamGuest->setGoalDifference($teamGuest->getGoalDifference() + $differenceTemGuest);

        $teamHome->save();
        $teamGuest->save();
    }
}