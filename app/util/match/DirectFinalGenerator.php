<?php

class DirectFinalGenerator
{
    public static function generate(Category $category)
    {
        $matches = Match::getFinalOfCategory($category);
        $teams = Team::getTeamsOfCategory($category->getId());
        $matches[0]->setTeamHome($teams[0]);
        $matches[0]->setTeamGuest($teams[1]);
        $matches[0]->save();
    }
}