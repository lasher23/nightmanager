<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 04.11.2017
 * Time: 13:35
 */
class Finalgenerator
{
    private $category;

    function __construct(Category $category)
    {
        $this->category = $category;
    }

    function generate()
    {
        $semifinals = Match::getQuarterfinalsOfCategory($this->category);
        $finals = Match::getFinalOfCategory($this->category);
        $semifinal1 = $semifinals[0];
        $semifinal2 = $semifinals[1];
        foreach ($finals as $final) {
            if ($final->getHall()->getId() == '1') {
                $winner1 = $this->getWinnerTeam($semifinal1);
                $winner2 = $this->getWinnerTeam($semifinal2);
                $final->setTeamHome($winner1);
                $final->setTeamGuest($winner2);
                $final->save();
            } elseif ($final->getHall()->getId() == '2') {
                $winner1 = $this->getLooserTeam($semifinal1);
                $winner2 = $this->getLooserTeam($semifinal2);
                $final->setTeamHome($winner1);
                $final->setTeamGuest($winner2);
                $final->save();
            }
        }
    }

    private function getWinnerTeam(Match $match): Team
    {
        if ($match->getScoreTeamHome() > $match->getScoreTeamGuest()) {
            return Team::getById($match->getTeamHome()->getId());
        } elseif ($match->getScoreTeamHome() < $match->getScoreTeamGuest()) {
            return Team::getById($match->getTeamGuest()->getId());
        }
        return null;
    }

    private function getLooserTeam(Match $match): Team
    {
        if ($match->getScoreTeamHome() < $match->getScoreTeamGuest()) {
            return Team::getById($match->getTeamHome()->getId());
        } elseif ($match->getScoreTeamHome() > $match->getScoreTeamGuest()) {
            return Team::getById($match->getTeamGuest()->getId());
        }
        return null;
    }
}