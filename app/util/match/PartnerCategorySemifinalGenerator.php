<?php

class PartnerCategorySemifinalGenerator
{
    static function generate(Category $category){
        $categoryPartner = $category->getPartnerCategory();
        $matches = Match::getQuarterfinalsOfCategory($category);
        $teams = Team::getTeamsOfCategory($category->getId());
        $teamsPartner = Team::getTeamsOfCategory($categoryPartner->getId());

        if (sizeof($matches) > 0) {
            $matches[0]->setTeamHome($teams[0]);
            $matches[0]->setTeamGuest($teamsPartner[1]);
            $matches[0]->setHall(Hall::getById(1));
            $matches[0]->setScoreTeamHome(0);
            $matches[0]->setScoreTeamGuest(0);
            $matches[0]->save();

            $matches[1]->setTeamHome($teamsPartner[0]);
            $matches[1]->setTeamGuest($teams[1]);
            $matches[1]->setHall(Hall::getById(2));
            $matches[1]->setScoreTeamHome(0);
            $matches[1]->setScoreTeamGuest(0);
            $matches[1]->save();
        }
    }
}