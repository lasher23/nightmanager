<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 03.11.2017
 * Time: 22:04
 */
class PointCalculator
{
    private $match = null;
    function __construct(Match $match)
    {
        $this->match = $match;
    }

    public function getMatch()
    {
        return $this->match;
    }
    public function calculatePoints()
    {
        var_dump($this->getMatch()->getScoreTeamHome() === $this->getMatch()->getScoreTeamGuest());
        if ($this->getMatch()->getScoreTeamHome() === $this->getMatch()->getScoreTeamGuest()) {
            $this->getMatch()->getTeamHome()->setPoints($this->getMatch()->getTeamHome()->getPoints() + 1);
            $this->getMatch()->getTeamHome()->save();
            $this->getMatch()->getTeamGuest()->setPoints($this->getMatch()->getTeamGuest()->getPoints() + 1);
            $this->getMatch()->getTeamGuest()->save();
        } else if ($this->getMatch()->getScoreTeamHome() > $this->getMatch()->getScoreTeamGuest()) {
            $this->getMatch()->getTeamHome()->setPoints($this->getMatch()->getTeamHome()->getPoints() + 2);
            $this->getMatch()->getTeamHome()->save();
        } else if ($this->getMatch()->getScoreTeamGuest() > $this->getMatch()->getScoreTeamHome()) {
            $this->getMatch()->getTeamGuest()->setPoints($this->getMatch()->getTeamGuest()->getPoints() + 2);
            $this->getMatch()->getTeamGuest()->save();
        }
    }
}