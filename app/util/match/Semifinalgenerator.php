<?php

/**
 * Created by PhpStorm.
 * User: Nici
 * Date: 04.11.2017
 * Time: 10:45
 */
class Semifinalgenerator
{
    private $category;

    function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function generate()
    {
        $teams = Team::getTeamsOfCategory($this->category->getId());
        $matches = Match::getQuarterfinalsOfCategory($this->category);
        if (sizeof($matches) > 0) {
            $matches[0]->setTeamHome($teams[0]);
            $matches[0]->setTeamGuest($teams[3]);
            $matches[0]->setHall(Hall::getById(1));
            $matches[0]->setScoreTeamHome(0);
            $matches[0]->setScoreTeamGuest(0);
            $matches[0]->save();

            $matches[1]->setTeamHome($teams[1]);
            $matches[1]->setTeamGuest($teams[2]);
            $matches[1]->setHall(Hall::getById(2));
            $matches[1]->setScoreTeamHome(0);
            $matches[1]->setScoreTeamGuest(0);
            $matches[1]->save();
        }
    }
}