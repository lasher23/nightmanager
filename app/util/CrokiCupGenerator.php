<?php

class CrokiCupGenerator
{
    public static function generate(Category $category)
    {
        $categories = Category::getCategoriesByCrokiParent($category->getId());
        $teams = Team::getTeamsOfCategory($category->getId());
        $firstHalfTeams = array_slice($teams, 0, sizeof($teams) / 2);
        $secondHalfTeams = array_slice($teams, sizeof($teams) / 2);

        self::moveTeamsToCategory($firstHalfTeams, $categories[0]);
        self::moveTeamsToCategory($secondHalfTeams, $categories[1]);
        self::generateMatches($categories[0], 1, 2);
        self::generateMatches($categories[1], 2, 1);
    }

    private static function generateMatches(Category $category, int $firstHall, int $secondHall)
    {
        $teams = Team::getTeamsOfCategory($category->getId());
        $matches = Match::getMatchesByHallAndCategoryInGroupgame($firstHall, $category->getId());
        $match1 = $matches[0];
        $match1->setTeamHome($teams[0]);
        $match1->setTeamGuest($teams[5]);
        $match1->save();
        $match2 = $matches[1];
        $match2->setTeamHome($teams[4]);
        $match2->setTeamGuest($teams[9]);
        $match2->save();
        $match3 = $matches[2];
        $match3->setTeamHome($teams[1]);
        $match3->setTeamGuest($teams[6]);
        $match3->save();
        $match4 = $matches[3];
        $match4->setTeamHome($teams[3]);
        $match4->setTeamGuest($teams[8]);
        $match4->save();
        $match5 = $matches[4];
        $match5->setTeamHome($teams[2]);
        $match5->setTeamGuest($teams[7]);
        $match5->save();
        $matchesHall2 = Match::getMatchesByHallAndCategoryInGroupgame($secondHall, $category->getId());
        $match6 = $matchesHall2[0];
        $match6->setTeamHome($teams[5]);
        $match6->setTeamGuest($teams[4]);
        $match6->save();
        $match7 = $matchesHall2[1];
        $match7->setTeamHome($teams[6]);
        $match7->setTeamGuest($teams[0]);
        $match7->save();
        $match8 = $matchesHall2[2];
        $match8->setTeamHome($teams[9]);
        $match8->setTeamGuest($teams[3]);
        $match8->save();
        $match9 = $matchesHall2[3];
        $match9->setTeamHome($teams[7]);
        $match9->setTeamGuest($teams[1]);
        $match9->save();
        $match10 = $matchesHall2[4];
        $match10->setTeamHome($teams[8]);
        $match10->setTeamGuest($teams[2]);
        $match10->save();
    }

    private static function moveTeamsToCategory($teams, Category $category)
    {
        foreach ($teams as $team) {
            $team->setCategory($category);
            $team->save();
        }
    }
}