<?php
require 'core/bootstrap.php';
session_start();
if (!$_SESSION['role']) {
    require 'app/controller/AccountController.php';
} else {

    $uri = $_GET['uri']??'';
    if($uri===''){
        SiteOpener::openUserSpecificDefaultSite($_SESSION['role']);
    }else{
        require $router->getRoute($uri);
    }
}