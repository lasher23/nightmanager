<?php
$router = new Router([
    'Team' => 'app/controller/TeamController.php',
    'Category' => 'app/controller/CategoryController.php',
    'Match' => 'app/controller/MatchController.php',
    'IncreaseScore' => 'app/controller/IncreaseMatchController.php',
    'DecreaseScore' => 'app/controller/DecreaseMatchController.php',
    'FinishMatch' => 'app/controller/FinishMatchController.php',
    'Generator' => 'app/controller/GeneratorController.php',
    'UndoMatch' => 'app/controller/MatchUndoController.php',
    'HallChooser' => 'app/controller/HallChooserController.php',
    'Logout' => 'app/controller/LogoutController.php',
]);